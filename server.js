var path = require("path");
var express = require("express");

var logger = require("morgan");
var bodyParser = require("body-parser"); // simplifies access to request body

var app = express();  // make express app
var http = require('http').Server(app);  // inject app into the server

app.use(express.static(__dirname + '/assets'));
//app.use(express.static(__dirname + '/views'));

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// 2 create an array to manage our entries
var entries = [];
app.locals.entries = entries; // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"));     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }));

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  // response.render("SumnimaWebsite");
  // console.log("hi")
  response.sendFile(__dirname+"/assets/SumnimaWebsite.html")
});
app.get("/SumnimaWebsite", function (request, response) {
  response.render("SumnimaWebsite");
});
app.get("/about", function (request, response) {
  response.render("SumnimaWebsite");
});
app.get("/calculator", function (request, response) {
  response.render("Calculation");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/index", function (request, response) {
  // console.log("skfrguist")
  response.render("index");
});
app.get("/contact.html", function (request, response) {

  response.render("Contact");
});



app.post("/contact.html", function(request, response){
  var api_key = 'key-81c10634bea4999de2ff6f71a8decc76';
  var domain = 'sandbox325764343eb449b1bb5d1ac15a501b60.mailgun.org'; 
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Mailgun Sandbox <postmaster@sandbox325764343eb449b1bb5d1ac15a501b60.mailgun.org>',
    to: 'Sumnima Rana <sumnimarana1@gmail.com>',
    subject: "Hello,Sumnima! This is a website request",
    text: "Name: " + request.body.a1 + " " +request.body.a2 + "\nMessage: " + request.body.message + "\nContact info: " + request.body.a3
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error){
      //response.send("Mail Sent");
     // for(i=0; i<20; i++){     
        console.log("Your messsage has been sent");
          // window.alert("your message has been sent");
         response.redirect("contact.html"); 

     // }
      // response.redirect("/views/Contact.ejs"); 
      // window.alert("your message has been sent");
    }
    else{
      //alert("Please try again");
    response.send("Mail not sent");
    }
  });
});
// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  console.log(request.body);
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/index");  // where to go next? Let's go to the home page :)
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('A03 project listening on http://127.0.0.1:8081/');
});
